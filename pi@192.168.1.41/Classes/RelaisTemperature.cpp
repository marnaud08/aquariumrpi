/*
 * RelaisTemperature.cpp
 *
 *  Created on: 16 déc. 2017
 *      Author: michel
 */

#include "RelaisTemperature.h"

RelaisTemperature::RelaisTemperature(InterfaceRS232 *portcom,QObject *parent):QObject(parent) {
	// TODO Auto-generated constructor stub
	mport=portcom;
/*	   regactionneur=new char[6];
	    regactionneur[0]='#';
	    regactionneur[2]='0';
	    regactionneur[3]=13;
	    regactionneur[1]='E';
*/
}

RelaisTemperature::~RelaisTemperature() {
	// TODO Auto-generated destructor stub
}

void RelaisTemperature::commandeRelais(bool commandepompe){
    if (commandepompe){
     mport->regactionneur[2]|=2;
     commandepompe=false;
    }
    else{
        mport->regactionneur[2]&=0xFD;
        commandepompe=true;
    }
    mport->writeBuf(mport->regactionneur,4);
    mport->getMessageVoie(ligne);
}

void RelaisTemperature::lireMessage(char *l){
	for (int i=0;i<6;i++)
		l[i]=ligne[i];
}


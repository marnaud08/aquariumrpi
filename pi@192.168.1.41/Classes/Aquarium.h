/**
 *  Copyright (C) 2017  michel  (michel@btssn.delattre.com)
 *  @file         Aquarium.h
 *  @brief        
 *  @version      0.1
 *  @date         29 déc. 2017 23:26:01
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Aquarium.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _AQUARIUM_H
#define _AQUARIUM_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++

// Includes qt
#include <QMainWindow>

// Includes application
#include "ui_Aquarium.h"
#include "Capteur.h"
#include "InterfaceRS232.h"
#include "RelaisPompe.h"
#include "RelaisTemperature.h"
#include "battement.h"
#include "basedonnees.h"
// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief 
 *  Description detaillee de la classe.
 */
class Aquarium:public QMainWindow, private Ui_Aquarium
{
	Q_OBJECT
	InterfaceRS232 *mport;
	Capteur *mcapteur;
	double consigneteau;
    RelaisPompe *mpompe;
    RelaisTemperature *mchauffage;
    Battement *mmiseajour;
    bool commandepompe;
	BaseDonnees *m_bd;
public :
    /**
     * Constructeur
     */
    Aquarium();
    /**
     * Destructeur
     */
    ~Aquarium();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;

    // Methodes privees
signals:
// signaux générés    
private slots:
    // definition des slots privées
	//void on_Lire_clicked();
	void on_ConsigneTeau_sliderReleased();
	void supervision();
	void on_Relais_Pompe_clicked();

private slots:
     // definition des slots publics
    
};

// Methodes publiques inline
// ex :
// inline void Aquarium::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Aquarium::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _AQUARIUM_H


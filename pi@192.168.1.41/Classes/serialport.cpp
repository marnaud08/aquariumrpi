#include "serialport.h"
#include <QDebug>
#include <QThread>
#include <QTime>

SerialPort::SerialPort(QObject *parent) :
QSerialPort(parent) {

    messageretour= new char[200];
    longueur=0;
    connect(this,SIGNAL(readyRead()),this,SLOT(getMessage()));
    toutlu=false;
}

bool SerialPort::setOpenPort(QString *port){
    //Ouverture du port et configuration du port
    this->setPortName(*port);
    this->close();
        this->setBaudRate(115200);
        this->setDataBits(Data8);
        this->setParity(NoParity);
        this->setStopBits(TwoStop);
       // this->setFlowControl(NoFlowControl);
       // this->setDataTerminalReady(true);
    if (!this->isOpen())
        openport=this->open(QIODevice::ReadWrite);
    else openport=true;
        this->setBaudRate(115200);
        this->setDataBits(Data8);
        this->setParity(NoParity);
        this->setStopBits(TwoStop);
       // this->setFlowControl(NoFlowControl);
       // this->setDataTerminalReady(true);
    messagelu=false;
    return openport;

}

SerialPort::~SerialPort() {
    // TODO Auto-generated destructor stub
    delete []messageretour;
        this->flush(); //If any data was written, this function returns true; otherwise returns false.
    this->close();
}

unsigned char SerialPort::getNextByte(){
    char data;
 this->readData(&data,1);
 return (unsigned char)(data);
}


int SerialPort::writeBuf(char *buf,int l){
    int j=this->writeData(buf,l);
    this->waitForBytesWritten(100);
    this->waitForReadyRead(100);
    return j;
}


bool SerialPort::getOpen(){
    return openport;
}

void SerialPort::setRazMessage(){
    str.clear();
}

QByteArray SerialPort::getMessageB(){
	return str;
}


int SerialPort::getMessageVoie(char * voie){
//return this->readLineData(voie,6);
	//while(!messagelu);
    messagelu=false;

    for(int i=0;i<6;i++){
        voie[i]=messageretour[i];
    }
    return longueur;
}

void SerialPort::getMessage(){
   str += this->readAll();

   qDebug()<<str;
}

void SerialPort::getMessagetoMulti(char *message){

    this->flush();
    this->write(message);
    this->setDataTerminalReady(true);

}

void SerialPort::setReponse(bool rep){
	reponse =rep;
}

bool SerialPort::getReponse(){
        return toutlu;
}

void SerialPort::setLongueur(int l){
    longueur=l;
}

int SerialPort::getLongueur(){
    return longueur;
}


/*
 *  Copyright (C) 2017  michel  (michel@btssn.delattre.com)
 *  @file         AquariumTest.h
 *  Classe        Aquarium
 *  @note         Classe en charge des tests unitaires
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _AQUARIUMTEST_H
#define _AQUARIUMTEST_H

// Includes system C

// Includes system C++
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Includes qt

// Includes application
#include "Aquarium.h"

class AquariumTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(AquariumTest);
    CPPUNIT_TEST(testMainView);
    CPPUNIT_TEST_SUITE_END();

public :
    void setUp();
    void tearDown();
    void testMainView();

    void testConstructor();
    void testUnitaire1();

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives

    // Methodes privees
};

#endif  // _AQUARIUMTEST_H



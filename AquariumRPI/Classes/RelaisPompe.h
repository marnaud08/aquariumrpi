/*
 * RelaisPompe.h
 *
 *  Created on: 16 déc. 2017
 *      Author: michel
 */

#ifndef RELAISPOMPE_H_
#define RELAISPOMPE_H_
#include <QObject>
//#include "Actionneur.h"
#include "InterfaceRS232.h"

class RelaisPompe: public QObject {
	Q_OBJECT
	InterfaceRS232 *mport;
//    char *regactionneur;
    char ligne[10];
public:
	RelaisPompe(InterfaceRS232 *portcom,QObject *parent=0);
	virtual ~RelaisPompe();
	void commandeRelais(bool);
	void lireMessage(char *);
};

#endif /* RELAISPOMPE_H_ */

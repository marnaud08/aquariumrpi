#include "basedonnees.h"
//#include "qsql_mysql.h"
#include <QDebug>
#include <QtWidgets/QMessageBox>
//#include<QMessageBox>


BaseDonnees::BaseDonnees(QObject *parent) :
    QObject(parent)
{
       db=QSqlDatabase::addDatabase("QMYSQL");
       bvalid=db.isValid();
       baseok=false;
       id=1;
}

bool BaseDonnees::getValid()
{
	return bvalid;
}

/******
 * 	path = "MyProjetOceanVital";
 * ma_base = new BaseDonnees(this);
 * ma_base->connecter(path);
 *
 * ********Méthode qui permet d'ouvrir et se connecter à la base de données*****/
bool BaseDonnees::connecter(QString path)
{
	    //chemin d'acces a la bdd mesures
	    db.setDatabaseName(path);
	    //lieu de la base de donnees
	    db.setHostName("localhost");
	    //nom d'utilisateur
	    db.setUserName("root");
	    //mot de passe
	    db.setPassword("snir");

	    //si la bdd n'est pas ouverte
		if( !db.open() )
		{
		  QMessageBox::critical(0, "Erreur de connexion de la base de donnees", db.lastError().text());
			return false;
		}
		qDebug( "Connexion reussie!" ); // message pour l'utilisateur
		id=this->getNBMesures()+1;
			return true;

}


/**************Creation de table*******/
bool BaseDonnees::creerTable()
{
    QSqlQuery qry; //declaration de variable
    qry.prepare( "CREATE TABLE PanneauBonus (id INT PRIMARY KEY AUTO_INCREMENT, DateAcqui DATE NOT NULL, HeureAcqui TIME NOT NULL, Energie SMALLINT, Intensite SMALLINT, Tension SMALLINT, Temperature SMALLINT)" );
                    //(IF NOT EXISTS)preparation de la creation d'une table avec ses differentes proprietes en fonction des champs
    //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
    {
      QMessageBox::critical(0, "Erreur lors de la création de la table", qry.lastError().text());
      return false;
    }
      qDebug() << "Creation de la table reussie!"; // message pour l'utilisateur
        return true;
}

/**************Insertion de valeur********
 *
 * ma_base->inserer(puissance, intensite, tension, temperature, "INSERT INTO Panneau1 (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");
 *
 *
 * */
bool BaseDonnees::inserer(double tempint, double tempext, double niveaueau, double ph, double luminosite)
{
   QSqlQuery qry;
   id++;
   time_t now = time(NULL); // declaration de now qui permet de recuperer la valeur instantanée de la date et de l'heure

   char ValD[20];           // creation d'un tableau de 20 colonnes afin de stocker la date et l'heure
   strftime(ValD, 20, "%Y-%m-%d-%H:%M:%S", localtime(&now)); //recuperation de la date et heure

   char ValH[20];
   strftime(ValH, 20, "%H:%M:%S", localtime(&now)); // recuperation de l'heure


 qry.prepare("INSERT INTO mesures (id,time, tempint, tempext, nivh2o, ph, lum) VALUES (?, ?,?,?,?,?,?)");
// qry.prepare("INSERT INTO PanneauBonus (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature) VALUES ( ?,?,?,?,?,?)");
    // preparation à l'insertion de valeur
 qry.addBindValue(id);
 qry.addBindValue(ValD); //valeur liee dans l'ordre
 qry.addBindValue(tempint);
 qry.addBindValue(tempext);
 qry.addBindValue(niveaueau);
 qry.addBindValue(ph);
 qry.addBindValue(luminosite);

  //Si la commande contenu dans le prepare ne s'execute pas
 if( !qry.exec() )
   {
     QMessageBox::critical(0, "Erreur lors de l insertion", qry.lastError().text());
     return false;
   }
 else
   {
     qDebug( "Insertion reussie!" ); // message pour l'utilisateur
     return true;
   }
}

/********************Insertion d'un fichier csv***************************/
bool BaseDonnees::fichier()
{
    QSqlQuery qry;
    qry.prepare("LOAD DATA LOCAL INFILE '/home/local/Documents/retry.csv' INTO TABLE MyProjetOceanVital.PanneauBonus FIELDS TERMINATED BY ',' ENCLOSED BY ' ' LINES TERMINATED BY '\n' (DateAcqui, HeureAcqui, Energie, Intensite, Tension, Temperature)");
        //preparation a l'insertion d'un fichier csv
    if( !qry.exec() )
    {
    	QMessageBox::critical(0, "Erreur lors de l insertion", qry.lastError().text());
    	return false;
    }
    else
    {
    	qDebug() << "Insertion du fichier reussie"; // message pour l'utilisateur
    	return true;
    }
}


int BaseDonnees::getNBMesures(){
    QSqlQuery qry;
    int r;
    qry.prepare( "SELECT * FROM mesures" );
       // preparation de la selection de tout le contenu de la table

     //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
      {
      QMessageBox::critical(0, "Erreur lors de l affichage", qry.lastError().text());
      return false;
      }

    else
    {
      qDebug( "mesures selectionne" ); // message pour l'utilisateur

      QSqlRecord rec = qry.record(); //encapsule les caractéristique de la bdd

      int cols = rec.count(); // retourne le nombre de colonnes
      QString val;
      //Permet l'affichage en fonction des lignes
      for( r=0; qry.next(); r++ )
        //for( int c=0; c<cols; c++ )
        //{
	//	    val = val + QString( "Ligne %1, %2: %3 \n" ).arg( r ).arg( rec.fieldName(c) ).arg( qry.value(c).toString());
        //}
	qDebug()<<r;
      return r;
     }

}



/**************Affichage du contenu de la table*******/
bool BaseDonnees::afficher()
{
    QSqlQuery qry;
    qry.prepare( "SELECT * FROM mesures" );
       // preparation de la selection de tout le contenu de la table

     //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
      {
      QMessageBox::critical(0, "Erreur lors de l affichage", qry.lastError().text());
      return false;
      }

    else
    {
      qDebug( "mesures selectionne" ); // message pour l'utilisateur

      QSqlRecord rec = qry.record(); //encapsule les caractéristique de la bdd

      int cols = rec.count(); // retourne le nombre de colonnes
      QString val;
      //Permet l'affichage en fonction des lignes
      for( int r=0; qry.next(); r++ )
        for( int c=0; c<cols; c++ )
        {
		    val = val + QString( "Ligne %1, %2: %3 \n" ).arg( r ).arg( rec.fieldName(c) ).arg( qry.value(c).toString());
        }
      QMessageBox::information(0, "Affichage",val);
      return true;
     }
}

/**************Affichage du contenu de la table******
QString BaseDonnees::LireBase()
{
       QString val;
    QSqlQuery qry;
    qry.prepare( "SELECT * FROM mesures" );
       // preparation de la selection de tout le contenu de la table

     //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
      {
      QMessageBox::critical(0, "Erreur lors de l affichage", qry.lastError().text());
      return val;
      }

    else
    {
      qDebug( "mesures selectionne" ); // message pour l'utilisateur

      QSqlRecord rec = qry.record(); //encapsule les caractéristique de la bdd

      int cols = rec.count(); // retourne le nombre de colonnes
 
      //Permet l'affichage en fonction des lignes
      for( int r=0; qry.next(); r++ )
        for( int c=0; c<cols; c++ )
        {
		    val = val + QString( "Ligne %1, %2: %3 \n" ).arg( r ).arg( rec.fieldName(c) ).arg( qry.value(c).toString());
        }
      return val;
     }
}
*/



/**************Changement valeur**************/
bool BaseDonnees::modifVal()
{
    QSqlQuery qry;
    qry.prepare( "UPDATE PanneauBonus SET Energie = '12345' WHERE id = 2" );
        // preparation de la mise à jour d'une ligne de la table

     //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
      {
      QMessageBox::critical(0, "Erreur lors de la modification", qry.lastError().text());
      return false;
      }

    else
      {
      qDebug( "Reussite de la modification de la valeur!" ); // message pour l'utilisateur
      return true;
      }
}

/**************Suppression de la ligne numero index*****/
bool BaseDonnees::supLigne(int index)
{
    QSqlQuery qry;

    qry.prepare( "DELETE FROM mesures WHERE id =?");
        //preparation de la suppression d'une ligne de la table
    qry.addBindValue(index);

     //Si la commande contenu dans le prepare ne s'execute pas
    if( !qry.exec() )
      {
      QMessageBox::critical(0, "Erreur lors de la suppression", qry.lastError().text());
      return false;
      }
    else
      {
        qDebug( "Ligne Supprimee!" ); // message pour l'utilisateur
         return true;
      }
}

/**************Suppression de la table*****/
bool BaseDonnees::supTable()
{
    QSqlQuery qry;
    qry.prepare("DROP TABLE mesures");
        //preparation de la suppression de la table

     //Si la commande contenu dans le prepare ne s'execute pas
     if(!qry.exec())
     {
         QMessageBox::critical(0, "Erreur lors de la suppression de la table", qry.lastError().text());
         return false;
     }
       else
     {
         qDebug( "Table Supprimee!" ); // message pour l'utilisateur
         return true;
     }
}

/**************Fermeture base de données************************/
bool BaseDonnees::fermer()
{
    db.close(); //fermeture de la base de données
    qDebug("La base de donnees est fermée, au revoir"); // message pour l'utilisateur
    return true;
}


BaseDonnees::~BaseDonnees(){
    if(db.open())
       {
      //qDebug() << "Connexion en cours sur " << q2c(db.hostName()) << std::endl;
       db.close();
       }
}

#ifndef SERIALPORT_H_
#define SERIALPORT_H_
#include <QSerialPort>
//#include <QtCharts/QChartView>
//#include <QtCharts/QSplineSeries>
#include <QObject>


#define IDLENGTHSTD 3
#define IDLENGTHEXT 8




class SerialPort :public QSerialPort{
     Q_OBJECT
     bool openport;
     char nblue;
    int longueur;
    bool messagelu;
    bool reponse;
    char *messageretour;
    QByteArray  str;
    bool toutlu;
public:
    SerialPort(QObject *parent = 0);
    virtual ~SerialPort();
    unsigned char getNextByte();
    bool setOpenPort(QString *port);
    void setRazMessage();
    bool getOpen();
    void getMessagetoMulti(char *message);
    int getMessageVoie(char * voie);
    void setReponse(bool rep);
    bool getReponse();
    QByteArray getMessageB();
    void setLongueur(int l);
    int getLongueur();
    int writeBuf( char *buf,int l);
    QByteArray *dataBuffer;
    signals:
        void confirm();
private slots:
        void getMessage();
};

#endif /* SERIALPORT_H_ */

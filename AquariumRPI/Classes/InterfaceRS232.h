/*
 * InterfaceRS232.h
 *
 *  Created on: 27 déc. 2017
 *      Author: michel
 */

#ifndef CLASSES_INTERFACERS232_H_
#define CLASSES_INTERFACERS232_H_

#include "serialport.h"

class InterfaceRS232: public SerialPort {

public:
	   char *regactionneur;
	InterfaceRS232(QObject *parent = 0);
	virtual ~InterfaceRS232();
};

#endif /* CLASSES_INTERFACERS232_H_ */

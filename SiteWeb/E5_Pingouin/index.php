<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<!--Ajout des variables PHP de gestion des serveurs de données-->
		<?php include("config/config.php");?>
		<title><?php echo ($sitename); ?></title>
		<link rel="icon" href="images/favicon.ico" />
		<!--Integration du fichier de style CSS-->
		<link rel="stylesheet" type="text/css" href="css/style.css" />
	</head>
	
	<!--Description du contenu de la page Web-->
	<body>
		<div id="container">

			<?php
				// Insertion de l'entête de page avec le logo et le bandeau
				include("config/header.php");
			?>

			<!--Description de l'affichage des caractéristiques du bassin des pingouins-->
			<div class="bassin">
				<h2>Bassin des Pingouins</h2>

				<div class="Mesure">
					<h3>CARACTERISTIQUES ACTUELLES DU BASSIN</h3>
					<?php
						// Connexion au serveur MySQL distant
						$conn_pingouin = mysql_connect($IP_bassin_pingouin, $nom_bassin_pingouin, $pass_bassin_pingouin) or die (mysql_error());
						// Sélection de la base de données distante sur laquelle travailler pour le test de connexion
						mysql_select_db($nom_bdd_bassin_pingouin, $conn_pingouin);

						$sql = "SELECT * FROM ".$nom_table_Bassin." ORDER BY id DESC LIMIT 1";
						$req = mysql_query($sql) or die('Erreur de connection SQL<br />'.$sql.'<br />'.mysql_error());
						$data = mysql_fetch_assoc($req);

						echo "Heure :" . $data['time'];
						echo "<br/>Température extérieure : ".$data['tempext']." °C";
						echo "<br/>Température intérieure : ".$data['tempint']." °C";
						echo "<br/>Niveau : ".$data['nivh2o']." cm";
						echo "<br/>PH : ".$data['ph'];
						echo "<br/>Lumière : ".$data['lum']." lux";
					?>
				</div>

				<div class="Webcam">
					<h3>WEBCAM</h3>
					
					<img src="http://"<?php echo $ipcam_pingouin ?>":8080/videostream.cgi?user=admin" height="150">
				</div>
			</div>
		</div>
	</body>

	<!--Insertion de la description de bas de page-->
	<?php include("config/footer.php"); ?>
</html>
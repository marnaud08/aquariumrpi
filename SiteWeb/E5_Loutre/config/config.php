<?php

	/*
	Page de configuration des caractéristiques des pages de supervision
	Cette page contient les caractéristiques (à compléter):
	- du serveur (nom du site / l'adresse du serveur en local / l'adresse du serveur en extérieur)
	- de la base de données hébergée sur le serveur (nom de l'utilisateur de la base / mot de passe / nom de la bdd / nom des tables)
	- des bases de données tampon contenues dans les différentes cartes contrôlant les bassins
	- des livecam permettant l'observation des bassins
	*/

	$sitename = "SDS Aquarium - Epreuve E5"; // Nom du site
	
	$nom_bdd_bassin_pingouin = ""; // Nom de la BDD contenant les caractéristiques du bassin pingouin
	$IP_bassin_pingouin = "";
	$nom_bassin_pingouin = "";
	$pass_bassin_pingouin = "";

	$nom_bdd_bassin_loutre = ""; // Nom de la BDD contenant les caractéristiques du bassin loutre
	$IP_bassin_loutre = "";
	$nom_bassin_loutre = "";
	$pass_bassin_loutre = "";

	$nom_bdd_bassin_poisson = ""; // Nom de la BDD contenant les caractéristiques du bassin poisson
	$IP_bassin_poisson = "";
	$nom_bassin_poisson = "";
	$pass_bassin_poisson = "";

	$nom_table_Bassin = ""; // Nom de la table contenant les caractéristiques des bassins
	
	$ipcam_pingouin = ""; // IP de la caméra en local
	$ipcam_loutre = ""; // IP de la caméra en local
	$ipcam_poisson = ""; // IP de la caméra en local
?>
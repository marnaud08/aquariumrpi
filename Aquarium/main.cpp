#include "Aquarium.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Aquarium w;
    w.show();
    return a.exec();
}

#ifndef AQUARIUM_H
#define AQUARIUM_H

#include <QMainWindow>
#include "Capteur.h"
#include "InterfaceRS232.h"
#include "RelaisPompe.h"
#include "RelaisTemperature.h"
#include "battement.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Aquarium; }
QT_END_NAMESPACE

class Aquarium : public QMainWindow
{
    Q_OBJECT
    InterfaceRS232 *mport;
    Capteur *mcapteur;
    double consigneteau;
    RelaisPompe *mpompe;
    RelaisTemperature *mchauffage;
    Battement *mmiseajour;
    bool commandepompe;
public:
    Aquarium(QWidget *parent = nullptr);
    ~Aquarium();

private slots:
    void on_Relais_Pompe_clicked();
    void supervision();

    void on_Relais_Temperature_clicked();

    void on_ConsigneTeau_sliderReleased();

private:
    Ui::Aquarium *ui;
};
#endif // AQUARIUM_H

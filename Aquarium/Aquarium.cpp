#include "Aquarium.h"
#include "ui_Aquarium.h"
#include <QDebug>
#define PORT "/dev/ttyUSB0"
//#define PORT "/dev/ttyACM1"

Aquarium::Aquarium(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Aquarium)
{
    ui->setupUi(this);
    mport=new InterfaceRS232(this);
    QString port=PORT;
    mport->setOpenPort(&port);
   mcapteur = new Capteur(mport,this);
   mchauffage = new RelaisTemperature(mport,this);
   this->mpompe=new RelaisPompe(mport,this);
   QColor vert(Qt::green);
   QPalette palette;
   palette.setColor(QPalette::Button,QColor(Qt::red));
   commandepompe=true;
   ui->Relais_Temperature->setPalette(palette);
   ui->Relais_Pompe->setStyleSheet("QPushButton {"
           "background-color: blue;"
           "border-style: outset;"
           "border-width: 2px;"
         "border-radius: 10px;"
          "border-color: beige;"
           "font: bold 14px;"
           "min-width: 10em;"
           "padding: 6px;"
       "}");
   ui->Relais_Temperature->setStyleSheet("QPushButton { background-color: blue; }");
   consigneteau=15;
   mmiseajour = new Battement(this);
   qDebug()<<"battement lancer";
    connect(this->mmiseajour,SIGNAL(miseajour()),this,SLOT(supervision()));
    qDebug()<<"connect  fait";
     mmiseajour->startb();
   qDebug()<<"programme lancer";
}

Aquarium::~Aquarium()
{
    delete mcapteur;
    delete mchauffage;
    delete mpompe;
    mmiseajour->quit();
    if(!mmiseajour->wait(3000)) //Wait until it actually has terminated (max. 3 sec)
    {
        mmiseajour->terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
        mmiseajour->wait(); //We have to wait again here!
    }

    delete mmiseajour;
    delete mport;
//	delete m_bd;
    delete ui;
}


void Aquarium::on_Relais_Pompe_clicked()
{
    mpompe->commandeRelais(commandepompe);
    if (commandepompe){
        commandepompe=false;
  //  	   this->Relais_Pompe->setStyleSheet("QPushButton { background-color: green; }");

        ui->Relais_Pompe->setText("Arrêt Pompe");
    }
    else{
        commandepompe=true;
 //	   this->Relais_Pompe->setStyleSheet("QPushButton { background-color: black; }");
        ui->Relais_Pompe->setText("Commande Pompe");
    }
}

void Aquarium::on_Relais_Temperature_clicked()
{
 /*   double valcapteurs[5];QString texte;
    mcapteur->lireCapteurs(valcapteurs);
    ui->lineTempEau->setText(texte.setNum(valcapteurs[0]));
    ui->lineTempAmb->setText(texte.setNum(valcapteurs[1]));
    ui->lineLumiere->setText(texte.setNum(valcapteurs[2]));
    ui->linePH->setText(texte.setNum(valcapteurs[3]));
*/
}

void Aquarium::supervision(){
       char ligne[10];

 double valcapteurs[5];QString texte;
 mcapteur->lireCapteurs(valcapteurs);
 ui->lineTempEau->setText(texte.setNum(valcapteurs[0]));
 ui->lineTempAmb->setText(texte.setNum(valcapteurs[1]));
 ui->lineLumiere->setText(texte.setNum(valcapteurs[3]));
 ui->linePH->setText(texte.setNum(valcapteurs[2]));
 if (this->consigneteau>valcapteurs[0]){
     ui->Relais_Temperature->setStyleSheet("QPushButton { background-color: red; }");
     mchauffage->commandeRelais(true);
     //niveau=0;
 }
 else {
     ui->Relais_Temperature->setStyleSheet("QPushButton { background-color: green; }");
     mchauffage->commandeRelais(false);
     //niveau=1.0;
 }
    //int ta=mtempambiante->lireCapteur(1);




//    mcapteur->lireCapteur(VOIENIVEAUEAU,&niv);
    if (valcapteurs[4]>2000)
        ui->lineNivh2o->setText("Non atteind");
    else ui->lineNivh2o->setText("atteind");





//	m_bd->inserer(valtempamb,valtempeau,niveau,valph,luminosite);

}


void Aquarium::on_ConsigneTeau_sliderReleased()
{
    consigneteau=ui->ConsigneTeau->value();
    QString text;
    ui->lineConsigneEau->setText(text.setNum(consigneteau));
}

/*
 * Capteur.h
 *
 *  Created on: 21 déc. 2016
 *      Author: michel
 */

#ifndef CLASSES_CAPTEUR_H_
#define CLASSES_CAPTEUR_H_
#include "serialport.h"
#include <QObject>
#include <QTimer>
#define TC1 19.8
#define VOT 400
#define PHOFFSET 0
#define RLUMIERE 68

enum VoieCapteurs {VOIETEAU,VOIETAMBIANTE,VOIELUMINOSITE=4,VOIEPH=8,VOIENIVEAUEAU=10};

class Capteur : public QObject{
		Q_OBJECT
		SerialPort *mport;
	    char *regcapteur;
	    char ligne[15];;
		QTimer *timer;
		bool fintempo;
public:
	Capteur(SerialPort *,QObject *parent=0);
	virtual ~Capteur();
    bool lireCapteur(int voie);
    void LireValeursCapteurs(QByteArray st,int *teau,int *tamb,int *ph,int *lum,int * niveau);
    void lireCapteurs(double *valnum);
	void lireMessage(char *);
public slots:
	void finTempo();
};

#endif /* CLASSES_CAPTEUR_H_ */

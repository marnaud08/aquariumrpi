/**
 *  Copyright (C) 2017  michel  (michel@btssn.delattre.com)
 *  @file         Aquarium.cpp
 *  @brief        
 *  @version      0.1
 *  @date         29 déc. 2017 23:26:01
 *
 *  Description detaillee du fichier Aquarium.cpp
 *  Fabrication   gcc (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609 
 *  @todo         Liste des choses restant a faire.
 *  @bug          29 déc. 2017 23:26:01 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <QDebug>
//#define PORT "/dev/ttyUSB0"
#define PORT "/dev/ttyACM0"
// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Aquarium.h"

/**
 * Constructeur
 */
Aquarium::Aquarium()
{
	this->setupUi(this);
        mport=new InterfaceRS232(this);
    QString port=PORT;
    mport->setOpenPort(&port);
   mcapteur = new Capteur(mport,this);
   mchauffage = new RelaisTemperature(mport,this);
   this->mpompe=new RelaisPompe(mport,this);
   QColor vert(Qt::green);
   QPalette palette;
   palette.setColor(QPalette::Button,QColor(Qt::red));
   commandepompe=true;
   this->Relais_Temperature->setPalette(palette);
   this->Relais_Pompe->setStyleSheet("QPushButton {"
           "background-color: blue;"
           "border-style: outset;"
           "border-width: 2px;"
         "border-radius: 10px;"
          "border-color: beige;"
           "font: bold 14px;"
           "min-width: 10em;"
           "padding: 6px;"
       "}");
   this->Relais_Temperature->setStyleSheet("QPushButton { background-color: blue; }");
   consigneteau=15;
//	m_bd=new BaseDonnees(this);
//	m_bd->connecter("BDDloutre");
  mmiseajour = new Battement(this);
  qDebug()<<"battement lancer";
   connect(this->mmiseajour,SIGNAL(miseajour()),this,SLOT(supervision()));
   qDebug()<<"connect  fait";
    mmiseajour->startb();/**/
  qDebug()<<"programme lancer";

}

/**
 * Destructeur
 */
Aquarium::~Aquarium()
{
	delete mcapteur;
	delete mchauffage;
	delete mpompe;
	mmiseajour->quit();
	if(!mmiseajour->wait(3000)) //Wait until it actually has terminated (max. 3 sec)
	{
	    mmiseajour->terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
	    mmiseajour->wait(); //We have to wait again here!
	}

	delete mmiseajour;
	delete mport;
//	delete m_bd;
}

void Aquarium::supervision(){
    double valcapteurs[5];QString texte;
    mcapteur->lireCapteurs(valcapteurs);
    this->lineTempEau->setText(texte.setNum(valcapteurs[0]));
    this->lineTempAmb->setText(texte.setNum(valcapteurs[1]));
    this->lineLumiere->setText(texte.setNum(valcapteurs[3]));
    this->linePH->setText(texte.setNum(valcapteurs[2]));
    if (this->consigneteau>valcapteurs[0]){
        this->Relais_Temperature->setStyleSheet("QPushButton { background-color: red; }");
        mchauffage->commandeRelais(true);
        //niveau=0;
    }
    else {
        this->Relais_Temperature->setStyleSheet("QPushButton { background-color: green; }");
        mchauffage->commandeRelais(false);
        //niveau=1.0;
    }
       if (valcapteurs[4]>2000)
           this->lineNivh2o->setText("Non atteind");
       else this->lineNivh2o->setText("atteind");

//        m_bd->inserer(valcapteurs[1],valcapteurs[0],valcapteurs[4],valcapteurs[2],valcapteurs[3]);
}


void Aquarium::on_Relais_Pompe_clicked(){
    char ligne[10];
    mpompe->commandeRelais(commandepompe);
    if (commandepompe){
    	commandepompe=false;

        this->Relais_Pompe->setText("Arrêt Pompe");
    }
    else{
    	commandepompe=true;
        this->Relais_Pompe->setText("Commande Pompe");
    }

}




void Aquarium::on_ConsigneTeau_sliderReleased(){
	consigneteau=this->ConsigneTeau->value();
	QString text;
	this->lineConsigneEau->setText(text.setNum(consigneteau));
}

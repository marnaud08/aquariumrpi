/*
 * RelaisTemperature.h
 *
 *  Created on: 16 déc. 2017
 *      Author: michel
 */

#ifndef RELAISTEMPERATURE_H_
#define RELAISTEMPERATURE_H_

//#include "Actionneur.h"
#include "InterfaceRS232.h"
#include <QObject>

class RelaisTemperature: public QObject {
	Q_OBJECT
	InterfaceRS232 *mport;
 //   char *regactionneur;
    char ligne[10];
public:
	RelaisTemperature(InterfaceRS232 *,QObject *parent=0);
	virtual ~RelaisTemperature();
	void commandeRelais(bool);
	void lireMessage(char *);
};

#endif /* RELAISTEMPERATURE_H_ */

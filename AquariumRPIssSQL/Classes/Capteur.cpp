/*
 * Capteur.cpp
 *
 *  Created on: 21 déc. 2016
 *      Author: michel
 */

#include "Capteur.h"
#include <QDebug>
#include <QTime>

Capteur::Capteur(SerialPort *portcom,QObject *parent):QObject(parent) {
	// TODO Auto-generated constructor stub
	mport=portcom;
	   regcapteur=new char[6];
	    regcapteur[0]='#';
	    regcapteur[2]='1';
	    regcapteur[3]=13;
	    regcapteur[1]='L';
//            QObject::connect(timer, SIGNAL(timeout()), this,SLOT(finTempo()));
}

Capteur::~Capteur() {
	// TODO Auto-generated destructor stub
	//delete mmk2;
}
void Capteur::finTempo(){
    static int i=0;int t;
    i++;
   fintempo=true;
   qDebug()<<"temporisation"<<i;
}

bool Capteur::lireCapteur(int voie){
    bool reponse;int l;int t;
	regcapteur[2]='0'+voie;
    mport->writeBuf(regcapteur,4);
    mport->setLongueur(0);
    return false;

}

void Capteur::lireMessage(char *l){
	for (int i=0;i<6;i++)
		l[i]=ligne[i];
}
void Capteur::LireValeursCapteurs(QByteArray st,int *teau,int *tamb,int *lum,int *ph,int * niveau){
for (int i=0;i<st.length();i++){
    if (st[i]=='L'){
        switch (st[i+1]-0x30){
        case VOIETEAU: *teau=((unsigned char)(st[i+2])<<8)+(unsigned char)(st[i+3]);break;
        case VOIETAMBIANTE: *tamb=((unsigned char)(st[i+2])<<8)+(unsigned char)(st[i+3]);break;
        case VOIELUMINOSITE: *lum=((unsigned char)(st[i+2])<<8)+(unsigned char)(st[i+3]);break;
        case VOIEPH: *ph=((unsigned char)(st[i+2])<<8)+(unsigned char)(st[i+3]);break;
        case VOIENIVEAUEAU: *niveau=((unsigned char)(st[i+2])<<8)+(unsigned char)(st[i+3]);break;
        }
        i=i+4;
    }
}
}

void Capteur::lireCapteurs(double *valnum){
    int teau,tambiante,luminosite,ph,phbis,niveaueau;
 for (int i=0;i<3;i++){
     this->lireCapteur(VOIETEAU);
    this->lireCapteur(VOIETAMBIANTE);
    this->lireCapteur(VOIELUMINOSITE);
    this->lireCapteur(VOIEPH);
     this->lireCapteur(VOIENIVEAUEAU);
 }

    QByteArray dataretour=mport->getMessageB();
    this->LireValeursCapteurs(dataretour,&teau,&tambiante,&luminosite,&ph,&niveaueau);
    mport->setRazMessage();
    valnum[0]=3.3*teau/(4.096*TC1)-VOT/TC1;
    qDebug()<<"t eau: "<<teau;

    valnum[1]=3.3*tambiante/(4.096*TC1)-VOT/TC1;
    qDebug()<<"t ambiant: "<<tambiante;

    valnum[2]=(double)(ph)*17.5/4096+PHOFFSET/*17.5*/;
    qDebug()<<"PH: "<<ph;

   valnum[3]=(double)(luminosite)*4.02832/RLUMIERE;
    qDebug()<<"luminosité: "<<luminosite;

     valnum[4]=(double)(niveaueau);
     qDebug()<<"Niveau eau: "<<niveaueau;
}

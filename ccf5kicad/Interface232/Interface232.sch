EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface232-rescue:adaptateur232-adaptateur232 J1
U 1 1 61D4E227
P 2450 2650
F 0 "J1" H 2425 2967 50  0000 C CNN
F 1 "adaptateur232" H 2425 2876 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W12.7mm_LongPads" H 2300 2650 50  0001 C CNN
F 3 "~" H 2300 2650 50  0001 C CNN
	1    2450 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 61D4FB45
P 3650 2700
F 0 "J2" H 3730 2742 50  0000 L CNN
F 1 "Conn_01x03" H 3730 2651 50  0000 L CNN
F 2 "Connector_Molex:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 3650 2700 50  0001 C CNN
F 3 "~" H 3650 2700 50  0001 C CNN
	1    3650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2600 3200 2600
Wire Wire Line
	3200 2600 3200 2200
Wire Wire Line
	3200 2200 2050 2200
Wire Wire Line
	2050 2200 2050 2550
Wire Wire Line
	2750 2550 3150 2550
Wire Wire Line
	2750 2650 3050 2650
Wire Wire Line
	2100 2550 2050 2550
Wire Wire Line
	3050 2800 3450 2800
Wire Wire Line
	3050 2650 3050 2800
Wire Wire Line
	3450 2700 3150 2700
Wire Wire Line
	3150 2700 3150 2550
$EndSCHEMATC
